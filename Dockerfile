FROM debian:jessie

ADD ardour-build-tools/win/x-mingw.sh /

RUN apt-get update -y -qq && apt-get install -y -qq \
	build-essential \
	git \
	autoconf \
	automake \
	libtool \
	pkg-config \
	curl \
	unzip \
	ed \
	yasm \
	cmake \
	ca-certificates \
	nsis \
	subversion \
	ocaml-nox \
	binutils-mingw-w64-i686 \
	binutils-mingw-w64-x86-64 \
	gcc-mingw-w64-base \
	mingw-w64-common \
	mingw-w64-i686-dev \
	mingw32-binutils \
	g++-mingw-w64-i686 \
	gcc-mingw-w64-i686 \
	mingw-w64-tools \
	mingw32

CMD [ "bin/bash", "x-mingw.sh" ]
